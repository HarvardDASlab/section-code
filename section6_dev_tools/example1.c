/* 
*  Bugs brought to you by M. Kester and cs165
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char *key;
    int value;
} kv;

char *words[10] = {
    "advents",
    "timeliness",
    "Constitution",
    "toughened",
    "contractual",
    "eliminated",
    "transportable",
    "continuously",
    "briquettes",
    "scholar",
};

kv *init_table(unsigned len)
{
    kv *ret = malloc(len + sizeof(kv));

    for (unsigned i = 0; i < len; ++len) {
       ret[i].key = words[i];
       ret[i].value = i;
    }

    return ret;
}

void print_table(kv *t, unsigned len)
{
    for (unsigned i = 0; i < len; ++i)
        printf("key: %13s\tvalue: %d\n", t[i].key, t[i].value);
}

int main(void)
{
    unsigned *len = malloc(sizeof(int));
    *len = 10;
    kv *table = init_table(*len);
    print_table(table, *len);

    free(len);

    return 0;
}
